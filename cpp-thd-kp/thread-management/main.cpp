#include <iostream>
#include <string>
#include <thread>
#include <cassert>
#include <vector>
#include <algorithm>

using namespace std;

void background_work(int id, chrono::milliseconds delay)
{
	cout << "Start THD#" << id << endl;

	for(int i = 0; i < 20; ++i)
	{
		cout << "THD#" << id << ": " << i << endl;
		this_thread::sleep_for(delay);
	}

	cout << "End of THD#" << id << endl;
}

void find_min(const vector<int>& data, int& min)
{
	auto min_it = min_element(data.begin(), data.end());

	min = *min_it;
}

class BackgroundWork
{
	const int id_;
public:
	BackgroundWork(int id) : id_{id}
	{}

	void operator()(chrono::milliseconds delay) const
	{
		cout << "Start BW#" << id_ << endl;

		for (int i = 0; i < 20; ++i)
		{
			cout << "BW#" << id_ << ": " << i << endl;
			this_thread::sleep_for(delay);
		}

		cout << "End of BW#" << id_ << endl;
	}
};

int main()
{
	thread thd0; // not-a-thread

	cout << "THD#0 id = " << thd0.get_id() << endl;

	thread thd1{ &background_work, 1, 200ms };

	const vector<int> data = { 434, 234, 15, 46, 25, 13, 88 };
	int min1, min2;

	thread thd2{ &find_min, cref(data), ref(min1) };
	thread thd3{ [&data, &min2] { find_min(data, min2); } };

	thread thd4{ BackgroundWork(2), 500ms };
	BackgroundWork bw(3);
	thread thd5{ ref(bw), 100ms };

	thd1.join();
	assert(!thd1.joinable());

	thd2.join();
	thd3.join();
	thd4.detach();
	assert(!thd4.joinable());

	thd5.join();

	cout << "min value = " << min1 << endl;
	cout << "min value = " << min2 << endl;
	
	system("PAUSE");
}