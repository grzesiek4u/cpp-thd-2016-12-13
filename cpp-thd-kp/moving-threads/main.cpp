#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <algorithm>

using namespace std;

void background_work(int id, chrono::milliseconds delay)
{
	cout << "Start THD#" << id << endl;

	for (int i = 0; i < 20; ++i)
	{
		cout << "THD#" << id << ": " << i << endl;
		this_thread::sleep_for(delay);
	}

	cout << "End of THD#" << id << endl;
}

thread create_thread()
{
	static int gen_id = 0;

	return thread{ &background_work, ++gen_id, 100ms };
}

template <typename...> struct Typelist;

class RaiiThread
{
	thread thd_;
public:
	// perfect forwarding constructor        
	template
		<
		typename... Args,
		typename = std::enable_if_t<!is_same<Typelist<RaiiThread>, Typelist<std::decay_t<Args>...>>::value>
		>
		RaiiThread(Args&&... args) : thd_{ forward<Args>(args)... }
	{}

	RaiiThread(const RaiiThread&) = delete;
	RaiiThread& operator=(const RaiiThread&) = delete;

	RaiiThread(RaiiThread&&) = default;
	RaiiThread& operator=(RaiiThread&&) = default;

	~RaiiThread()
	{
		if (thd_.joinable())
			thd_.join();
	}

	thread& get()
	{
		return thd_;
	}
};

int main()
{
	cout << "No of cores: " << max(1u, thread::hardware_concurrency()) << endl;

	{
		thread thd1 = create_thread();
		thread thd2 = create_thread();

		vector<thread> thds;

		thds.push_back(move(thd1));
		thds.push_back(move(thd2));
		thds.push_back(thread{ &background_work, 3, 400ms });
		thds.emplace_back(&background_work, 4, 400ms);

		for (auto& t : thds)
			t.join();
	}

	{
		thread thd1 = create_thread();
		RaiiThread thd2 = create_thread();

		vector<RaiiThread> thds;

		thds.push_back(move(thd1));
		thds.push_back(move(thd2));
		thds.push_back(thread{ &background_work, 3, 400ms });
		thds.emplace_back(&background_work, 4, 400ms);
	}
}