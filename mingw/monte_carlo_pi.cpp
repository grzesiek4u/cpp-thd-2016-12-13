#include <iostream>
#include <string>
#include <thread>
#include <algorithm>
#include <vector>
#include <numeric>
#include <random>

using namespace std;

void calc_hits(uint64_t no_of_iterations, uint64_t& hits)
{
	random_device rd;
	mt19937_64 rnd_engine(rd());
	uniform_real_distribution<double> rnd_distr(-1.0, 1.0);
	
	double x, y;	
	
	for (uint64_t i = 0; i < no_of_iterations; i++)
	{
		x = rnd_distr(rnd_engine);
		y = rnd_distr(rnd_engine);
		if (x*x + y*y <= 1)
		{
			hits++;
		}
	}
}

void calc_mc_pi_one_thread(uint64_t no_of_iterations)
{
	uint64_t hits = 0;

	auto start = chrono::high_resolution_clock::now();

	calc_hits(no_of_iterations, hits);

	double pi = 4.0 * (hits / static_cast<double>(no_of_iterations));

	auto end = chrono::high_resolution_clock::now();

	auto time = chrono::duration_cast<chrono::milliseconds>(end - start).count();

	cout << "Pi (single thread) = " << pi << " Time: " << time << "ms" << endl;
}

void calc_mc_pi_many_thread(uint64_t no_of_iterations)
{
	auto no_of_cores = max(1u, thread::hardware_concurrency());
	uint64_t no_of_iterations_per_thread = no_of_iterations / no_of_cores;
	uint64_t hits = 0;

	auto start = chrono::high_resolution_clock::now();

	vector<thread> thds(no_of_cores);
	vector<uint64_t> partial_hits(no_of_cores);
	
	for(unsigned int i = 0; i < no_of_cores; ++i)
	{
		thds[i] = thread{ &calc_hits, no_of_iterations_per_thread, ref(partial_hits[i]) };
	}

	for (auto& t : thds)
		t.join();

	hits = accumulate(partial_hits.begin(), partial_hits.end(), 0ull);

	double pi = 4.0 * (hits / static_cast<double>(no_of_iterations));

	auto end = chrono::high_resolution_clock::now();

	auto time = chrono::duration_cast<chrono::milliseconds>(end - start).count();

	cout << "Pi (" << no_of_cores << " threads) = " << pi << " Time: " << time << "ms" << endl;
}

int main()
{
	const uint64_t no_of_iterations = 50'000'000;
		
	calc_mc_pi_one_thread(no_of_iterations);
	calc_mc_pi_many_thread(no_of_iterations);

	//system("PAUSE");
}